#!/bin/ksh

# Intellectual property information START
# 
# Copyright (c) 2021 Ivan Bityutskiy 
# 
# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# 
# Intellectual property information END

# Description START
#
# The script creates text file fetch.txt
# with amount of words specified in outWordsAmount variable.
# Input file source.txt must contain 1 word per line.
# The script is used to fetch random words from input file
# and create a single line containing all words in output file.
# This script is written for OpenBSD's pdksh.
#
# Description END

# Shell settings START
umask 077
# Shell settings END

# Define functions START
function getRandom
{
  local eightBits
  read -u3 -- eightBits
  print -n -- "$eightBits"
}
# Define functions END

# Declare variables START
# source.txt is a text file with 1 word per line.
typeset inputFile='./source.txt'
# fetch.txt will contain single string of words, separated by spaces.
typeset outputFile='./fetch.txt'
# sedArgs will contain all arguments for sed
typeset sedArgs=''

# number of lines (1 word per line) in input file
integer inputLinesAmount=$(wc -l < $inputFile)
# number of words in output file
integer outWordsAmount=100
integer numCounter
integer dcResult
# Declare variables END

# BEGINNING OF SCRIPT
# start endless hexdump stream (8 bytes UInt per line) at file descriptor 3
hexdump -v -e '/8 "%u\n"' /dev/urandom |&
exec 3<&p
exec 4>&p

# start dc as a co-process
dc |&

for numCounter in $(jot $outWordsAmount)
do
  # print into dc: random8bitUInt % inputFileLinesAmount + 1 (reverse polish notation)
  print -p -- "$(getRandom) $inputLinesAmount % 1 + p"
  # read result from dc
  read -p -- dcResult
  # populate a string with arguments for sed
  sedArgs="$sedArgs -e ${dcResult}p"
done

# close hexdump stream
exec 3<&-
exec 4>&-
# close dc co-process
print -p -- 'q'

# sed returns alphabetically sorted output, sort -R randomizes it,
# tr transforms it into a single string
sed -n $sedArgs $inputFile |
  sort -R |
    tr '\n' ' ' >| $outputFile
# add EOL to output file
print -- '' >> $outputFile

print -u2 -- "\nFile \"$PWD/\033[91m${outputFile#*/}\033[0m\"\noverwritten successfully!\n"

# END OF SCRIPT

